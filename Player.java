import java.util.Scanner;
import java.util.*;
public class Player {
	private String name;
	private char initial;
	private char piece;
	private int score;
	private int move;

	public static void main(String[] args) {

	}
	public Player() {
		this.name = "NULL";
		this.initial = 'x';
		this.piece = 'x';
		this.score = 0;
		this.move = 0;
	}

	public Player(String name, char initial, char piece, int score, int move) {
		this.name = name;
		this.initial = initial;
		this.piece = piece;
		this.score = score;
		this.move = move;
	}

	// getters
	public String getName() {
		return this.name;
	}

	public char getInitial() {
		return this.initial;
	}

	public char getPiece() {
		return this.piece;
	}

	public int getScore() {
		return this.score;

	}

	public int getMove() {
		return this.move;
	}

	// setters
	public void setName(String name) {
		this.name = name;
	}
	public void setInitial(char initial) {
		this.initial = initial;
	}
	public void setPiece(char piece) {
		this.piece = piece;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public void setMove(int move) {
		this.move = move;
	}
    
    /**
	 * A function that finds the first character in the player's name and assigns it to the player.piece variable.
	 *
	 * @param Player player, Object player, named player
	 *
	 * @return player.piece, this is the piece that will be used during the tic-tac-toe game
	 */ 

	public static char findPiece(Player player) {
		player.piece= player.getName().charAt(0);
		System.out.println(player.piece);
		return player.piece;
	}
    
    /**
	 * Asks for the player name
	 *
	 * @param String player, Usually either one or two, this value is the player's number.
	 *
	 * return String player
	 */ 
	public static String pNameAssignment(String player) {
	        Scanner in = new Scanner(System.in);
		System.out.printf("Player %s what is your name?\n", player);
	        player = in.nextLine();
		return player;
	}
	
}

import java.util.Scanner;
import java.util.Random;

public class TicTacJava{
		public  static char[] board = new char[]{'0','1','2','3','4','5','6','7','8'}; 

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		mainMenu(in);	
	}
	
	/**
	 * Runs the pNameAssignment() and findPiece() functions, getting the name and piece char from the two players.
	 *
	 * @param scanner in
	 */  
	public static void twoPlayer(Scanner in) {
		Player p1 = new Player();
		Player p2 = new Player();
		p1.setName(p1.pNameAssignment("one"));
		p2.setName(p2.pNameAssignment("two"));
	    p1.setInitial(p1.findPiece(p1));	
	    p2.setInitial(p2.findPiece(p2));
    	gameMoves(in, p1, p2);
	}

	

	
	public static void playABot(int roboDifficulty) {
		double randomNumber  = Math.random();
		randomNumber *= randomNumber * 10;
		int randomChoice = (int) randomNumber;
		System.out.println(randomChoice);
		Bot bot = new Bot();
		bot.name = "Fred";
		System.out.printf("Hi my name is %s you're going down!\n", bot.name);
		//bot.initial = findPiece(bot);
		if(roboDifficulty == 1) {
			System.out.println("EASY");
		} else if (roboDifficulty == 2) {
			System.out.println("HARD");
		} else {
			System.out.println("INVALID");
		}

		
	}

	public static void highScores() {

	}
	/*
	 * Checks to make sure the player has chosen a location that hasn't already been taken by either player
	 *
	 * @param
	 * @param
	 * @param
	 * @param
	 * @param
	 */
	public static void validMove(Scanner in, char[] board, Player p1, Player p2, int move) {
        while (board[p1.getMove()] == p1.getPiece() || board[p1.getMove()] == p2.getPiece()) {
                 System.out.println("This location has already been taken, please choose another.");
                 printBoard(board);
                 System.out.printf("%s's turn: ", p1.getName());
                 p1.setMove(in.nextInt());
               }
   	 	}
	
	public static void gameMoves(Scanner in, Player p1, Player p2 ) {
		int turns = 0;
        	printBoard(board);   

		for(int i =0; i <= 8; i++){
     		int choice = -1;
     		winBoard(in,board,p1);
     		winBoard(in,board,p2);
         if((turns % 2) == 0) {  
         	
             System.out.printf("%s's turn: ", p1.getName());
             System.out.println("");
             p1.setMove(in.nextInt());
  	        // checkChoiceBounds(in, choice);
            validMove(in, board, p1, p2, p1.getMove());
           
             board[p1.getMove()] = p1.getPiece();
             turns++;
             printBoard(board);   
          } else if((turns % 2) == 1) {
          	 
             System.out.printf("%s's turn: ", p2.getName()); 
             System.out.println("");

             p2.setMove(in.nextInt());
            // checkChoiceBounds(in, choice);
            validMove(in, board, p2, p1, p2.getMove());
           
             board[p2.getMove()] = p2.getPiece();
             turns++;
             printBoard(board);   
           }
          
   }
	}
	public static int mainMenu(Scanner in) {
		int choice = 9;
		System.out.println("Welcome to |TIC-TAC-TOE|");
		System.out.println("What would you like to do?(Choose a number)");
		System.out.println("1. Two Player\n2. Play a bot\n3. High Score\n4. Exit\n");
		System.out.print("Choice: ");
		choice = in.nextInt();
		int roboDifficulty= 0;
		switch(choice) {
			case 1: board = new char[]{'0','1','2','3','4','5','6','7','8'}; 
					twoPlayer(in);
					break;
			case 2: System.out.printf("1.EASY\n2.HARD\n");
				roboDifficulty = in.nextInt();
				playABot(roboDifficulty);
					break;
			case 3: highScores();
					break;
			case 4: System.out.printf("...exiting");
					break;
			default: System.out.printf("testing");
					break;
		}
		return choice;
	}


	/*
	 * Checks and prints who the winner of the game is. Also restarts game.
	 */
	public static int winBoard(Scanner in, char[] board, Player player){
        char choice;
       if((board[0] == player.getPiece()) && (board[1] == player.getPiece()) && (board[2] == player.getPiece()) ||
          (board[0] == player.getPiece()) && (board[3] == player.getPiece()) && (board[6] == player.getPiece()) ||
          (board[0] == player.getPiece()) && (board[4] == player.getPiece()) && (board[8] == player.getPiece()) ||
          (board[1] == player.getPiece()) && (board[4] == player.getPiece()) && (board[7] == player.getPiece()) ||
          (board[2] == player.getPiece()) && (board[5] == player.getPiece()) && (board[8] == player.getPiece()) ||
          (board[2] == player.getPiece()) && (board[4] == player.getPiece()) && (board[6] == player.getPiece()) ||
          (board[3] == player.getPiece()) && (board[4] == player.getPiece()) && (board[5] == player.getPiece()) ||
          (board[6] == player.getPiece()) && (board[7] == player.getPiece()) && (board[8] == player.getPiece())) {
        System.out.printf("%s won!\n", player.getName());
        //player.setScore();
	System.out.println();
        mainMenu(in);
   
     	}
     	return 1;
    }

	/**
	 * Prints the board of the tic-tac-toe game.
	 *
	 * @param char[] board, is the array of numbers to print out on the board
	 */
	public static void printBoard(char[] board){
		// prints board out
	    System.out.printf("| %c | %c | %c |\n", board[0], board[1], board[2]);
	    System.out.printf("-------------\n");
	    System.out.printf("| %c | %c | %c |\n", board[3], board[4], board[5]);
	    System.out.printf("-------------\n");
	    System.out.printf("| %c | %c | %c |\n", board[6], board[7], board[8]);
	}
}

/*public static int checkChoiceBounds(Scanner in ,int choice) {
	
             while(choice < 0 || choice > 8) { 
                 if(choice < 0){
                     System.out.println("Your choice was too low, please choose 0-8.");
                     choice = in.nextInt();
                 }else if(choice > 8) {
                     System.out.println("Your choice was too high, please choose 0-8.");
                     choice = in.nextInt();
                 } 
              }
              return choice;
    }*/
